package com.sang.jenklnstest;

public class Core {

    public static String businessLogic(String valueString){

        String result = "";

        int value = 0;

        try{
            value = Integer.parseInt(valueString);
        }catch (NumberFormatException npe){
            value = -1;
        }

        if(value >= 1 && value <=10){
            result = "Hello Jenkins";
        }else if (value > 10){
            result = "Hello Java";
        }else{
            result = "Hello";
        }

        return result;
    }
}
