package com.sang.jenklnstest;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class CoreTest {

    @Test
    public void businessLogicTest_success_case1() {

        String result = Core.businessLogic("1");

        assertEquals("Hello Jenkins", result);
    }

    @Test
    public void businessLogicTest_success_case2() {

        String result = Core.businessLogic("11");

        assertEquals("Hello Java", result);
    }

    @Test
    public void businessLogicTest_success_case3() {

        String result = Core.businessLogic("0");

        assertEquals("Hello", result);
    }

    @Test
    public void businessLogicTest_failure_case4() {

        String result = Core.businessLogic("String");

        assertEquals("Hello", result);
    }
}